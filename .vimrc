" LJB-Vim -1.2.0 ~~ Nice logo edition

" ██╗██╗██╗██╗██╗██╗██╗██╗██╗██╗██╗██╗██╗██╗
" ╚═╝╚═╝╚═╝╚═╝╚═╝╚═╝╚═╝╚═╝╚═╝╚═╝╚═╝╚═╝╚═╝╚═╝
" ██╗   ██╗██╗ ████████╗    ██████╗  ██████╗ 
" ╚██╗ ██╔╝██║██╔═██╔═██╗   ██╔══██╗██╔════╝
"  ╚████╔╝ ██║██║ ██║ ██║   ██████╔╝██║   
"   ╚██╔╝  ██║██║ ██║ ██║██╗██╔══██╗╚██████╗
"    ╚═╝   ╚═╝╚═╝ ╚═╝ ╚═╝╚═╝╚═╝  ╚═╝ ╚═════╝
" ██╗██╗██╗██╗██╗██╗██╗██╗██╗██╗██╗██╗██╗██╗
" ╚═╝╚═╝╚═╝╚═╝╚═╝╚═╝╚═╝╚═╝╚═╝╚═╝╚═╝╚═╝╚═╝╚═╝

set nocompatible              " be iMproved, required
"filetype off                  " required
colorscheme solarized

"set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
  Plugin 'VundleVim/Vundle.vim'
"------------------=== Visuals ===--------------------
  Plugin 'altercation/vim-colors-solarized'   " solarized color scheme
  Plugin 'terryma/vim-smooth-scroll'          " Smooth scroll
  Plugin 'Lokaltog/powerline'                 " Powerline Fonts
  Plugin 'bling/vim-airline'                  " Airline
  Plugin 'vim-airline/vim-airline-themes'     " Airline Themes
"---------------=== Pandoc Support ===----------------
  Plugin 'vim-pandoc/vim-pandoc'              " vim pandoc integration
  Plugin 'vim-pandoc/vim-pandoc-syntax'
  Plugin 'vim-pandoc/vim-rmarkdown'
"---------------=== Latex Support ===-----------------
  Plugin 'lervag/vimtex'                      " better latex support in vim
  Plugin 'xuhdev/vim-latex-live-preview'      " Latex live preview support
"------------------=== Snippets ===-------------------
  Plugin 'garbas/vim-snipmate'                " Vim snippets integration (no snippets included)
  Plugin 'honza/vim-snippets'                 " Vim snippets package
"-------------------=== Utilities ===---------------------
  Plugin 'tpope/vim-commentary'               " Comment stuff out [TODO does this actually work?]
  Plugin 'tpope/vim-surround'                 " Surround like i t f but s
  Plugin 'tpope/vim-repeat'
  Plugin 'majutsushi/tagbar'                  " Class/module browser
  "Plugin 'aaronbieber/vim-quicktask'          " Quicktask (task manager)
"-------------------=== Other ===---------------------
  Plugin 'MarcWeber/vim-addon-mw-utils'       " snippets and dependencies
  Plugin 'tomtom/tlib_vim'
  Plugin 'jiangmiao/auto-pairs'
"-------------=== Languages support ===-----------------
  Plugin 'Valloric/YouCompleteMe'             " Autocomplete under cursor {'do': './install.py --clang-completer'} 
  Plugin 'w0rp/ale'                           " syntax checker
  Plugin 'Shougo/neocomplcache.vim'           " Better autocompletion
  Plugin 'octol/vim-cpp-enhanced-highlight'   " Better c++ syntax highligting
  Plugin 'jelera/vim-javascript-syntax'       " Better javascript syntax highligting

" DEPRICATED 
" (Working on removing as many plugins as possible while keeping a the nice parts at the moment)
" Plugin 'ervandew/supertab'                  " make YCM compatible with snippets (removed double tab support, didn't work well)
" Plugin 'scrooloose/nerdtree'                " Project and file navigation (made obsolete by i3wm workflow)
" Plugin 'kien/ctrlp.vim'                     " Fast transitions on project files (made obsolete by i3wm workflow)
" Plugin 'mitsuhiko/vim-sprkup'               " Sparkup(XML/jinja/htlm-django/etc.) support (not needed anymore)
" Plugin 'Rykka/riv.vim'                      " ReStructuredText plugin (not even sure what this plugin did anymore)
" Plugin 'vim-syntastic/syntastic'            " syntastic (too heavy, ale is good enough)
" Plugin 'davidhalter/jedi-vim'               " Python autocompletion, go to definition. (loaded too slowly, got anoying)
" Plugin 'bkad/CamelCaseMotion'               " camelcasemotion (tried it, didn't use it very much)
" Plugin 'posva/vim-vue'                      " Vue syntax highlighting (stopped using vue, didn't need it anymore)
call vundle#end()            " required
filetype plugin indent on    " required
filetype plugin on
" " Brief help
" " :PluginList       - lists configured plugins
" " :PluginInstall    - installs plugins; append `!` to update or just
" :PluginUpdate
" " :PluginSearch foo - searches for foo; append `!` to refresh local cache
" " :PluginClean      - confirms removal of unused plugins; append `!` to
" auto-approve removal
" "
" " see :h vundle for more details or wiki for FAQ
" " Put your non-Plugin stuff after this line

" General Settings
"make YCM compatible with UltiSnips (using supertab)
let g:ycm_key_list_select_completion = ['<C-n>', '<Down>']
let g:ycm_key_list_previous_completion = ['<C-S-n>', '<Up>']
let g:SuperTabDefaultCompletionType = '<C-m>'

" better key bindings for UltiSnipsExpandTrigger
" let g:UltiSnipsExpandTrigger = "<tab>"
" let g:UltiSnipsJumpForwardTrigger = "<tab>"
" let g:UltiSnipsJumpBackwardTrigger = "<s-tab>"

" cpp hightlighting custimizations
let g:cpp_class_scope_highlight = 1
let g:cpp_class_decl_highlight = 1
let g:cpp_member_variable_highlight = 1

"call camelcasemotion#CreateMotionMappings(';')

" Auto Pairs
let g:AutoPairsFlyMode=0
let g:AutoPairsFlyMode=0
let g:AutoPairsShortcutToggle="<M-C-9>"

syntax enable
let python_highlight_all = 1    
set encoding=utf-8

" incremental search
set incsearch                 

" highlight search results
" set hlsearch                  
"
" line number + relative numbers
set number relativenumber     
" soft wrap and easier movement through wrapped text
set linebreak
nnoremap j gj
vnoremap j gj
nnoremap k gk
vnoremap k gk

" always show status bar
set ls=2                      

" tabs and spaces handling
set expandtab                 

" tab 2 spaces
set tabstop=2
set softtabstop=2
set shiftwidth=2

set nocompatible
set clipboard=unnamedplus

" enable autocompletion
set wildmenu
set wildmode=list:longest,full

" toggle between paste and nopaste mode
set pastetoggle=<C-s>

" airline settings
let g:airline_theme='solarized'
let g:airline_powerline_fonts = 1

".tex files are interpreted as latex (not txt)
let g:tex_flavor = 'latex'

" MAPPINGS
" compile python
cnoremap <C-p> <space>!python3 % <cr>
cnoremap <C-p>i <space>!python3 -i % <cr>
" compile c++
cnoremap <C-g> !g++ -std=c++11 -o %.out %<cr>
cnoremap <C-g><C-g> !./%.out<cr>

" remap W, Wq, WQ 
cnoremap W w
cnoremap Wq wq
cnoremap WQ wq

" select all
inoremap <C-a> <esc> ggvG$

" remap escape to C-j for easy access
" noremap <C-j> <esc>
" inoremap <C-j> <esc>

" creates new line under current line
inoremap <C-l> <esc>A<cr>
nnoremap <C-l> <esc>A<cr><esc>

" swap dark and light theme on the fly
function! Swap_bg()
  if &background=="light"
    set background=dark
  elseif &background=="dark"
    set background=light
  endif
endfunction

nnoremap <C-i> :call Swap_bg()<cr>

" creates new line above current line
inoremap <C-o> <esc>I<cr><esc>ki
nnoremap <C-o> I<cr><esc>k

" smooth scroll
let scrolltime=15
nnoremap <silent> <c-u> :call smooth_scroll#up(&scroll, scrolltime, 2)<CR>
nnoremap <silent> <c-d> :call smooth_scroll#down(&scroll, scrolltime, 2)<CR>
nnoremap <silent> <c-b> :call smooth_scroll#up(&scroll*2,2*scrolltime, 4)<CR>
nnoremap <silent> <c-f> :call smooth_scroll#down(&scroll*2,2*scrolltime, 4)<CR>

set foldmethod=indent
