#!/bin/bash

export GZIP=-9
tar -cvzf back_up.tar.gz ~/{bin,Downloads,coding,.vimrc,documents}
