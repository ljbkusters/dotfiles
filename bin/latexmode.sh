#!/bin/bash

PPID_RAW=$PPID        #Gets process ID
PID=$(expr $PPID - 2) #Corrects to parent ID

i3-msg append_layout /home/linuxbook/.i3/latex-layout.json
exec urxvt &
exec urxvt &
exec urxvt -e ranger &

kill $PID  # Kill parent window
