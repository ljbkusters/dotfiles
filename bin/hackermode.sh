#!/bin/bash

i3-msg append_layout /home/linuxbook/.i3/i3-hacker-layout.json
exec urxvt -e cmatrix &
exec urxvt -e vis &
exec urxvt -e htop &
exec urxvt &

kill -1 $PPID
